﻿using UnityEngine;
using UnityEngine.UI;

public class BlinkingText : MonoBehaviour
{
    [SerializeField] private Text _text = default;

    [SerializeField] private float _option = 1.0f;

    private void Update()
    {
        if (_text.enabled == false)
        {
            return;
        }

        Color color = _text.color;
        color.a += Time.deltaTime * _option;
        color.a = Mathf.Clamp(color.a, 0, 1.0f);
        if (color.a >= 1.0f || color.a <= 0)
        {
            _option *= -1;
        }

        _text.color = color;
    }
}
