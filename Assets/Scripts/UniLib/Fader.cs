﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Fader : SingletonMonoBhv<Fader>
{
    /// <summary>
    /// フェード用描画域
    /// </summary>
    private Canvas _Canvas;
    private Image _Image;

    Fader() { }
    private void Awake()
    {
        DOTween.Init();
        Init();
    }

    //private bool _IsInit = false;
    public void Init()
    {
        //Debug.Log("Fader Init");
        GameObject fadeCanvasObj = new GameObject("FadeCanvas");
        _Canvas = fadeCanvasObj.AddComponent<Canvas>();
        fadeCanvasObj.AddComponent<GraphicRaycaster>();
        _Canvas.renderMode = RenderMode.ScreenSpaceOverlay;

        // 最前面に来るように
        _Canvas.sortingOrder = 1000;

        // フェード用のイメージ作成
        _Image = new GameObject("FadeImage").AddComponent<Image>();
        _Image.transform.SetParent(_Canvas.transform, false);
        _Image.rectTransform.anchoredPosition = Vector3.zero;
        _Image.rectTransform.sizeDelta = new Vector2(Screen.width, Screen.height);
        _Image.color = Color.clear;

        SetActiveFade(false);
    }

    private void Update() { }

    public void SetActiveFade(bool value)
    {
        _Canvas.gameObject.SetActive(value);
    }

    public void StartFade(float duration, Color startColor, Color targetColor, TweenCallback callback)
    {
        SetActiveFade(true);
        _Image.color = startColor;
        var seq = DOTween.Sequence();
        seq.Append(_Image.DOColor(targetColor, duration).SetEase(Ease.Linear));
        seq.AppendCallback(() => {
            callback();
        });
    }
    public void BlackOut(float duration, TweenCallback callback)
    {
        StartFade(duration, Color.clear, Color.black, callback);
    }
    public void BlackIn(float duration, TweenCallback callback)
    {
        StartFade(duration, Color.black, Color.clear, callback);
    }
    public void WhiteOut(float duration, TweenCallback callback)
    {
        StartFade(duration, Color.clear, Color.white, callback);
    }
    public void WhiteIn(float duration, TweenCallback callback)
    {
        StartFade(duration, Color.white, Color.clear, callback);
    }
}
