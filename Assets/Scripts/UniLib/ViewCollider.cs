﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
public class ViewCollider : MonoBehaviour {
    private Mesh _SphereMesh;

    // 表示色
    [Header("表示色")]
    public Color _Color = new Color(1, 0, 0, .5f);

    private void Awake() {
        _SphereMesh = GetComponentMesh(PrimitiveType.Sphere);
    }

    private Mesh GetComponentMesh(PrimitiveType type) {
        GameObject gameObject = GameObject.CreatePrimitive(type);
        Mesh mesh = gameObject.GetComponent<MeshFilter>().sharedMesh;
        DestroyImmediate(gameObject);

        return mesh;
    }

    private void OnDrawGizmos() {
        Gizmos.color = _Color;

        SphereCollider sc = GetComponent<SphereCollider>();
        if (sc && sc.enabled) {
            Vector3 offset = (transform.right * sc.center.x)
                           + (transform.up * sc.center.y)
                           + (transform.forward * sc.center.z);
            Vector3 scale = Vector3.one * sc.radius * 2;
            DrawMesh(_SphereMesh, offset, scale);
        }
    }

    private void DrawMesh(Mesh mesh, Vector3 offset, Vector3 scale) {
        Gizmos.DrawMesh(mesh, transform.position + offset,
                        transform.rotation, scale);
    }
}
