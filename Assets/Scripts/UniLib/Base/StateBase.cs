﻿using System.Collections;
using UnityEngine;

namespace State {
    /// <summary>
    /// ステート基底クラス
    /// </summary>
    public abstract class StateBase {
        public delegate void executeState();
        public executeState ExecDelegate;

        public bool StateEnd { set; get; }

        public StateBase(executeState exectute) {
            ChangeState(exectute);
        }

        public abstract void Execute();

        public void ChangeState(executeState execute) {
            ExecDelegate = execute;
            StateEnd = false;
        }
        public string GetStateName()
        {
            if (ExecDelegate == null)
            {
                return "";
            }
            return ExecDelegate.Method.Name;
        }
    }

    /// <summary>
    /// サブステート
    /// </summary>
    public class SubState : StateBase {
        public SubState(executeState execute) : base(execute) { }
        public override void Execute() {
            ExecDelegate?.Invoke();
        }
    }
}
