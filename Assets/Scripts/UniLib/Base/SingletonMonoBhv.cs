﻿using UnityEngine;
using System.Collections;

public class SingletonMonoBhv<T> : MonoBehaviour where T : MonoBehaviour {
    private static T _instance;
    public static T Inst {
        get {
            if (_instance == null) {
                _instance = (T)FindObjectOfType(typeof(T));
                if (_instance == null) {
                    // 見つからなかった場合
                    GameObject obj = new GameObject();
                    _instance = obj.AddComponent<T>();
                    obj.name = typeof(T).ToString();
                    DontDestroyOnLoad(obj);
                }
            }
            return _instance;
        }
    }
}
