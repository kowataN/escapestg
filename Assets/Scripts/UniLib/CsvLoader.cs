﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class CsvLoader {
	public enum TDelimiter {
		Comma, Tab
	}
	private char[] _Separators = {',', '\t'};
    protected Dictionary<int, List<string>> _Datas = new Dictionary<int, List<string>>();
    private List<string> _Headers = new List<string>();
    public List<string> Headers {
        get { return _Headers; }
        set { _Headers = value; }
    }

	/**
	 * @brief csvファイルを読み込みます
	 * @param path ファイルパス
	 */
	public virtual bool Load(string path, TDelimiter delimiter) {
        if (_Datas.Count > 0) {
            _Datas.Clear ();
		}

		TextAsset textasset = Resources.Load(path) as TextAsset;
		if (textasset == null) {
			return false;
		}
		StringReader reader = new StringReader(textasset.text);
		int linecount = 0;
		int nSep = (int)delimiter;
		while (reader.Peek() > -1) {
			string line = reader.ReadLine();
            // [/* ~ */]形式のコメントには対応しません
			if (line.IndexOf("//") >= 0) {
				// コメント行はスキップ
				continue;
			}
			string[] datas = line.Split(_Separators[nSep]);
			if (linecount == 0) {
                _Headers.AddRange(datas); // ヘッダーは別管理
			} else {
                int datano = int.Parse(datas[0]);
                if (_Datas.ContainsKey(datano)) {
                    _Datas.Remove(datano); // 既に存在した場合は削除してから追加
                }
                _Datas[datano] = new List<string>(datas);
			}
			++ linecount;
		}
        Resources.UnloadAsset(textasset);
		return true;
	}
	/**
	 * @brief csvデータを返します
	 * @return csvデータ
	 */
    public Dictionary<int, List<string>> GetDatas() {
        return _Datas;
	}
	/**
	 * @brief 該当レコードデータを返します
	 * @param[in] no 番号
	 * @return レコードデータ
	 */
    public List<string> GetData(int no) {
        return _Datas.ContainsKey(no) ? _Datas[no] : null;
	}
	/**
	 * @brief レコード件数を返します
	 * @return レコードデータ
	 */
	public int GetCount() {
        return _Datas.Count;
	}
	/**
	 * @brief 文字列からヘッダーのインデックスを返します
	 * @return ヘッダーインデックス。存在しない場合は-1を返す
	 */
	public int GetHeaderIndexFromString(string value) {
        return _Headers.IndexOf(value);
	}
}
