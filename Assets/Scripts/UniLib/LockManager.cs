﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockManager {
    private bool _isLock = false;

    /// <summary>
    /// ロックします
    /// </summary>
    public void Lock() { _isLock = true; }

    /// <summary>
    /// アンロックします
    /// </summary>
    public void UnLock() { _isLock = false; }

    /// <summary>
    /// ロック中かを返します
    /// </summary>
    /// <returns>true：ロック、false：アンロック</returns>
    public bool IsLock() { return _isLock; }

    /// <summary>
    /// アンロック中かを返します
    /// </summary>
    /// <returns>true：アンロック、false：ロック</returns>
    public bool IsUnLock() { return _isLock == false; }
}
