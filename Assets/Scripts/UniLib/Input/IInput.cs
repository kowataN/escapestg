﻿/// <summary>
/// 入力用インターフェースクラス
/// </summary>
interface IInput {
    /// <summary>
    /// 入力情報を更新します
    /// </summary>
    void UpdateInput();
    /// <summary>
    /// 入力情報の初期化を行います
    /// </summary>
    void InitInput();
}
