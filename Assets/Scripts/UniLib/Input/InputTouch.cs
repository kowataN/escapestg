﻿using UnityEngine;

/// <summary>
/// タッチ入力クラス
/// </summary>
public class InputTouch : IInput {
    /// <summary>
    /// 入力情報を更新します
    /// </summary>
    public void UpdateInput() {
        //LogView.Log("InputTouch::UpdateInput");
    }
    /// <summary>
    /// 入力情報を初期化します
    /// </summary>
    public void InitInput() {
        //LogView.Log("InputTouch::InitInput");
    }
}
