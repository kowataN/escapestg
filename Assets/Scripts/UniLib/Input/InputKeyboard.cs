﻿using UnityEngine;

/// <summary>
/// キーボード入力クラス
/// </summary>
public class InputKeyborad : IInput {
    /// <summary>
    /// 水平方向への移動量
    /// </summary>
    public float InputHorizontal { get; private set; }

    /// <summary>
    /// 垂直方向への移動量
    /// </summary>
    public float InputVertical { get; private set; }

    /// <summary>
    /// 入力情報を更新します
    /// </summary>
    public void UpdateInput() {
        InputHorizontal = Input.GetAxis("Horizontal");
        InputVertical = Input.GetAxis("Vertical");
    }

    /// <summary>
    /// Input の Key 情報を返します
    /// </summary>
    /// <returns><c>true</c>, if input key was gotten, <c>false</c> otherwise.</returns>
    public bool GetKey(KeyCode key) {
        return Input.GetKey(key);
    }

    /// <summary>
    /// Input の KeyDown 情報を返します
    /// </summary>
    /// <returns><c>true</c>, if key down was gotten, <c>false</c> otherwise.</returns>
    /// <param name="key">Key.</param>
    public bool GetKeyDown(KeyCode key) {
        return Input.GetKeyDown(key);
    }

    /// <summary>
    /// 入力情報を初期化します
    /// </summary>
    public void InitInput() {
        InputHorizontal = InputVertical = 0.0f;
    }
}
