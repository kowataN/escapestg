﻿using UnityEngine;

/// <summary>
/// マウス入力クラス
/// </summary>
public class InputMouse : IInput {
    private Vector3 _TouchPosition = Vector3.zero;
    public Vector3 TouchPosition { get { return _TouchPosition; }}

    /// <summary>
    /// 入力情報を更新します
    /// </summary>
    public void UpdateInput() {
        if (Input.GetMouseButtonDown(0)) {
            Debug.Log("InputMouse::GetMouseButtonDown(0)");
            _TouchPosition = Input.mousePosition;
            Debug.Log("Pos : " + _TouchPosition.ToString());
        }
        if (Input.GetMouseButtonUp(0)) {
            Debug.Log("InputMouse::GetMouseButtonUp(0)");
            InitInput();
        }
    }
    /// <summary>
    /// 入力情報を初期化します
    /// </summary>
    public void InitInput() {
        _TouchPosition = Vector3.zero;
    }
}
