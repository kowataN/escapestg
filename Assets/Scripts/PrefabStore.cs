using UnityEngine;

public class PrefabStore : SingletonMonoBhv<PrefabStore> { 
    [SerializeField] private GameObject _player = default;
    [SerializeField] private GameObject _enemy1 = default;
    [SerializeField] private GameObject _shot1 = default;

    public GameObject Player => _player;
    public GameObject Enemy1 => _enemy1;
    public GameObject Shot1 => _shot1;
}
