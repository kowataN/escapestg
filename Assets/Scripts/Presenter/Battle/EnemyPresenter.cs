﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using System;

public class EnemyPresenter : MonoBehaviour {
    [SerializeField] private CharacterView _view = default;
    private IDisposable _disposlable = default;

    public CharacterView View => _view;

    public void Initialize(GameObject target) {
        float interval = 0.2f;
        // 毎秒減算
        _disposlable = Observable.Timer(
                TimeSpan.FromSeconds(interval), TimeSpan.FromSeconds(interval))
            .Subscribe(_ => {
                Entity.ShotData shotData = new Entity.ShotData();
                ShotManager.Inst.CreateShot(shotData, _view, target);
            }).AddTo(_view.gameObject);
    }
}

