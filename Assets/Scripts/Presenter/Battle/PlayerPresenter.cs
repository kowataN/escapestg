﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using System;
using UnityEngine.InputSystem;

public class PlayerPresenter : MonoBehaviour {
    // 入力系
    [SerializeField] private PlayerInput _playerInput = default;
    private InputAction _move = default;

    private PlayerModel _model = default;
    public PlayerModel Model => _model;

    [SerializeField] private CharacterView _view = default;
    public CharacterView View => _view;

    [SerializeField] private KasuriPresenter _kasuriPresenter = default;
    [SerializeField] private UnitHitAreaPresenter _unitHitAreaPresenter = default;

    private void Awake() {
        var actionMap = _playerInput.currentActionMap;
        _move = actionMap["Move"];
    }

    public void Initialize(PlayerModel model) {
        _model = model;
        SetObservable();

        _kasuriPresenter?.Initialize(model);
        _unitHitAreaPresenter?.Initialize(model);
    }

    private void SetObservable() {
        // 方向キー入力
        this.UpdateAsObservable()
            .Subscribe(_ => {
                var move = _move.ReadValue<Vector2>();
                if (move.x == 0 && move.y == 0) {
                    _model.LatestLocalPos = _model.LocalPos;
                } else {
                    _model.AddLoadPos(move * Time.deltaTime);
                }
            }).AddTo(_view.gameObject);

        // キャラの移動
        _model.LocalPosObservable
            .Subscribe(x => _view.UpdateLocalPos(x))
            .AddTo(_view.gameObject);
    }

    IEnumerator WaitForBlink() {
        // 1秒間処理を止める
        yield return new WaitForSeconds(1);

        // １秒後ダメージフラグをfalseにして点滅を戻す
        _view.Sprite.color = new Color(1f, 1f, 1f, 1f);
    }
}

