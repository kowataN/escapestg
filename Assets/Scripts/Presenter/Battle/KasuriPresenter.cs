﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KasuriPresenter : MonoBehaviour {
    private PlayerModel _model = default;
    //[SerializeField] private KasuriView _View = default;

    public void Initialize(PlayerModel model) => _model = model;

    private void OnTriggerEnter2D(Collider2D collision) {
        // 自信の弾でカスらないように判定
        bool isHit = (collision.tag == Entity.TagName.EnemyBullet && gameObject.tag == Entity.TagName.PlayerKasuriArea)
                    || (collision.tag == Entity.TagName.PlayerBullet && gameObject.tag == Entity.TagName.EnemyKasuriArea);
        if (isHit) {
//            _model.AddShotGage(10);
        }
    }
}
