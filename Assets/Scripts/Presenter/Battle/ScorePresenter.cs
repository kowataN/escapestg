using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class ScorePresenter : MonoBehaviour
{
    [SerializeField] private ScoreView _view = default;
    private ScoreModel _model = default;
    private int _beforeTimer = 0;

    public void Initialize(long score, TimerModel timerModel) {
        _model = new ScoreModel();
        _model.Initialize(score);

        SetObservable(timerModel);
    }

    private void SetObservable(TimerModel timerModel) {
        timerModel.Timer.AsObservable()
            .Subscribe(x => {
                int diffTimer = x - _beforeTimer;
                _model.Score.Value += diffTimer * 100;
                _beforeTimer = x;
            }).AddTo(_view.gameObject);

        // 画面更新
        _model.Score.SubscribeToText(_view.Score);
    }
}
