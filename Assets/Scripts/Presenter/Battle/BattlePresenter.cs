﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class BattlePresenter : SingletonMonoBhv<BattlePresenter> {
    [SerializeField] private Transform _playerPos = default;
    [SerializeField] private Transform _shotParent = default;

    private GameObject _player = default;
    private PlayerPresenter _playerPresenter = default;
    private StateManager _stateMng = default;

    // タイマー
    [SerializeField] private int _timer = 0;
    [SerializeField] private TimerPresenter _timerPresenter = default;

    // スコア
    [SerializeField] private long _score = 0;
    [SerializeField] private ScorePresenter _scorePresenter = default;

    // タイムテーブル
    private TimeTablePresenter _timeTablePresenter = default;

    public EffectView EffectView => _effectView;
    [SerializeField] private EffectView _effectView = default;


    private void Awake() {
        ShotManager.Inst.CreateShotObjectPool(_shotParent);

        // ステートマシン初期化
        _stateMng = new StateManager();
        _stateMng.Initialize();
        this.UpdateAsObservable()
            .Subscribe(_ => {
                _stateMng.Execute();
            })
            .AddTo(this.gameObject);
    }

    public void Initialize() {
        if (_player != null) {
            Destroy(_player);
        }

        // タイマー初期化
        _timerPresenter?.Initial(_timer);
        // スコア初期化
        _scorePresenter?.Initialize(_score, _timerPresenter.Model);
        // タイムテーブル
        _timeTablePresenter = new TimeTablePresenter();
        _timeTablePresenter.Initial(_timerPresenter.Model);

        _player = GameObject.Instantiate<GameObject>(PrefabStore.Inst.Player,
            Vector2.zero, Quaternion.identity, _playerPos.transform);

        // プレイヤー初期化
        _playerPresenter = _player.GetComponentInChildren<PlayerPresenter>();
        PlayerModel playerModel = new PlayerModel();
        playerModel.Initialize(_player.transform.localPosition);

        _playerPresenter.Initialize(playerModel);

        // 試し
        {
            var enemy = GameObject.Instantiate<GameObject>(PrefabStore.Inst.Enemy1,
                Vector2.zero, Quaternion.identity);
            enemy.transform.localEulerAngles = new Vector3(0f, 0f, 180.0f);
            enemy.transform.localPosition = new Vector3(0f, 3.0f, 0f);
            EnemyPresenter enePre = enemy.GetComponentInChildren<EnemyPresenter>();
            if (enePre) {
                enePre.Initialize(_player);
            }
        }
    }

    public void GameStart() {
        _timerPresenter.StartTimer();
    }

    public void GameLoop() {
        _timeTablePresenter.Update();
    }
}
