﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class ShotManager : SingletonMonoBhv<ShotManager>
{
    public ObjectPool<GameObject> ShotObjectPool => _shotObjPool;
    private ObjectPool<GameObject> _shotObjPool = default;

    private Transform _shotParent = default;

    public void CreateShotObjectPool(Transform shotParent) {
        _shotParent = shotParent;

        // 後で別ファイルにする
        _shotObjPool = new ObjectPool<GameObject>(() => {
            // 生成処理
            return GameObject.Instantiate<GameObject>(PrefabStore.Inst.Shot1,
                Vector2.zero, Quaternion.identity, _shotParent);
        },
        target => {
            // 再利用
            target.SetActive(true);
        },
        target => {
            // プールに戻す
            target.SetActive(false);
        },
        target => {
            // 破棄処理
            Destroy(target);
        },
        true, 10, 20);
    }


    public void CreateShot(Entity.ShotData shotData, CharacterView view, GameObject targetUnit)
    {
        switch (shotData._Type)
        {
            case Entity.Constant.ShotType.Normal:
                CreateNormalShot(shotData, view, targetUnit);
                break;

            case Entity.Constant.ShotType.NormalPush:
                CreateNormalShot(shotData, view, targetUnit);
                break;

            case Entity.Constant.ShotType.Wide:
                Entity.WideShotData wideData = (Entity.WideShotData)shotData;
                CreateWideShot(wideData, view, targetUnit);
                break;

            case Entity.Constant.ShotType.Homing:
                Entity.HomingShotData homingData = (Entity.HomingShotData)shotData;
                CreateHomingShot(homingData, view, targetUnit);
                break;

            case Entity.Constant.ShotType.Bomb:
                break;
        }
    }

    private void CreateNormalShot(Entity.ShotData shotData, CharacterView view, GameObject targetUnit)
    {
        GameObject shotObj = _shotObjPool.Get();
        shotObj.transform.localPosition = view.ShotPos.position;
        shotObj.transform.localRotation = view.ShotPos.rotation;
        //GameObject shotObj = Instantiate(shotData._Prafab, view.ShotPos.position, view.ShotPos.rotation);
        ShotPresenter shotPre = shotObj.GetComponentInChildren<ShotPresenter>();
        shotPre.Initialize(shotData, targetUnit, shotObj.transform.localPosition);
    }

    /// <summary>
    /// ワイドショットを作成します。
    /// </summary>
    /// <param name="shotData"></param>
    /// <param name="view"></param>
    /// <param name="targetUnit"></param>
    private void CreateWideShot(Entity.WideShotData shotData, CharacterView view, GameObject targetUnit)
    {
        GameObject shotObj = _shotObjPool.Get();
        shotObj.transform.localPosition = view.ShotPos.position;
        shotObj.transform.localRotation = view.ShotPos.rotation;
        //GameObject shotObj = Instantiate(shotData._Prafab, view.ShotPos.position, view.ShotPos.rotation);
        ShotPresenter shotPre = shotObj.GetComponentInChildren<ShotPresenter>();

        shotPre.Initialize(shotData, targetUnit, shotObj.transform.localPosition);

        for (int i = 0; i < shotData._WayCount / 2; i++)
        {
            float addRot = shotData._AddRot * (i + 1);

            shotObj = _shotObjPool.Get();
            shotObj.transform.localPosition = view.ShotPos.position;
            shotObj.transform.localRotation = view.ShotPos.rotation;
            //shotObj = Instantiate(shotData._Prafab, view.ShotPos.position, view.ShotPos.rotation);
            shotObj.transform.Rotate(new Vector3(0, 0, addRot));
            shotPre = shotObj.GetComponentInChildren<ShotPresenter>();
            shotPre.InitializeWide(shotData, targetUnit, shotObj.transform.localPosition, addRot);

            shotObj = _shotObjPool.Get();
            shotObj.transform.localPosition = view.ShotPos.position;
            shotObj.transform.localRotation = view.ShotPos.rotation;
            //shotObj = Instantiate(shotData._Prafab, view.ShotPos.position, view.ShotPos.rotation);
            shotObj.transform.Rotate(new Vector3(0, 0, -addRot));
            shotPre = shotObj.GetComponentInChildren<ShotPresenter>();
            shotPre.InitializeWide(shotData, targetUnit, shotObj.transform.localPosition, -addRot);
        }
    }

    private void CreateHomingShot(Entity.HomingShotData shotData, CharacterView view, GameObject targetUnit)
    {
        GameObject shotObj = _shotObjPool.Get();
        shotObj.transform.localPosition = view.ShotPos.position;
        shotObj.transform.localRotation = view.ShotPos.rotation;
        //GameObject shotObj = Instantiate(shotData._Prafab, view.ShotPos.position, view.ShotPos.rotation);
        ShotPresenter shotPre = shotObj.GetComponentInChildren<ShotPresenter>();
        shotPre.InitializeHoming(shotData, targetUnit, shotObj.transform.localPosition);

    }
}
