using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UniRx;
using UnityEngine;

public class TimeTablePresenter 
{
    private Dictionary<int, List<EnemyModel>> _timeTableList = new Dictionary<int, List<EnemyModel>>();
    private List<int> _timeList = new List<int>();
    private TimerModel _timerModel = default;
    private int _crtEventTime = 0;
    private int _nextEventTime = 0;
    private int _crtEventIndex = 0;

    public void Initial(TimerModel timerModel) {
        _timerModel = timerModel;
        _crtEventIndex = 0;
        _crtEventTime = 0;

        CsvLoad();

        Debug.Log($"TimeTable:{_timeTableList.Count}");
    }

    public void StartTimeTable() {
        _crtEventIndex = 0;
        _crtEventTime = 0;
    }

    public void Update() {
        if (_timerModel.Timer.Value == _nextEventTime) {
            // イベント発行
            if (_timeTableList.ContainsKey(_timerModel.Timer.Value)) {
                _crtEventTime = _nextEventTime;
                var timeEvent = _timeTableList[_crtEventTime];
                Debug.Log($"イベント発行　Time[{_crtEventTime}]");

                _crtEventIndex++;
                if (_crtEventIndex < _timeList.Count) {
                    _nextEventTime = _timeList[_crtEventIndex];
                } else {
                    _nextEventTime = -1;
                }
            }
        }
    }

    private void CsvLoad() {
        TextAsset textasset = Resources.Load("TimeTable") as TextAsset;
        if (textasset == null) {
            return;
        }

        _timeTableList.Clear();

        StringReader reader = new StringReader(textasset.text);
        while (reader.Peek() > -1) {
            string line = reader.ReadLine();
            if (line.IndexOf("//") >= 0) {
                // コメント行はスキップ
                continue;
            }

            string[] datas = line.Split(',');
            int dataNo = int.Parse(datas[0]);
            if (!_timeTableList.ContainsKey(dataNo)) {
                _timeTableList[dataNo] = new List<EnemyModel>();
            }
            _timeTableList[dataNo].Add(
                new EnemyModel(
                    int.Parse(datas[1]),
                    new Vector2(float.Parse(datas[2]), float.Parse(datas[3])),
                    new Vector2(float.Parse(datas[4]), float.Parse(datas[5])),
                    int.Parse(datas[6])
                    ));

            if (!_timeList.Contains(dataNo)) {
                _timeList.Add(dataNo);
            }
        }
        Resources.UnloadAsset(textasset);
    }
}
