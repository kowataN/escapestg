﻿using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class ShotPresenter : MonoBehaviour {
    [SerializeField] private ShotView _view = default;
    [SerializeField] private ShotModel _model = default;

    /// <summary>
    /// 経過フレーム数
    /// </summary>
    private float _progessFrame = default;
    private float _startFrame = default;

    private bool _isObservable = false;

    public void Initialize(Entity.ShotData shotData, GameObject targetUnit, Vector2 objPos) {
        SetTag(targetUnit.tag);
        _model.Initialize(shotData, targetUnit, objPos);
        _model.SetRot(targetUnit.transform.localPosition);
        if (_isObservable == false) {
            _isObservable = true;
            SetObservable();
        }
    }

    public void InitializeWide(Entity.WideShotData shotData, GameObject targetUnit, Vector2 objPos, float addRot) {
        SetTag(targetUnit.tag);
        _model.Initialize(shotData, targetUnit, objPos);
        _model.SetRotWide(targetUnit.transform.localPosition, addRot);
        SetObservable();
    }

    public void InitializeHoming(Entity.HomingShotData shotData, GameObject targetUnit, Vector2 objPos) {
        SetTag(targetUnit.tag);
        _model.Initialize(shotData, targetUnit, objPos);
        _model.SetRot(targetUnit.transform.localPosition);
        _startFrame = Time.frameCount;
        SetObservableHoming();
    }

    private void SetTag(string tag) {
        this.tag = tag == "EnemyShip" ? "PlayerBullet" : "EnemyBullet";
    }

    private void SetObservable() {
        // 移動
        this.UpdateAsObservable()
            .Subscribe(_ => {
                _model.UpdatLocalPos();
                if (_model.LocalPos.x < -4.0f || _model.LocalPos.x > 4.0f
                    || _model.LocalPos.y < -2.0f || _model.LocalPos.y > 4.0f) {
                    ShotManager.Inst.ShotObjectPool.Release(_view.gameObject);
                    //Destroy(_view.gameObject);
                }
                _view.UpdateLocalPos(_model.LocalPos);
            }).AddTo(_view.gameObject);


        // 画面に反映
        //_model.LocalPosObservable
        //    .Subscribe(x => _view.UpdateLocalPos(x))
        //    .AddTo(_view.gameObject);

        // 回転
        this.UpdateAsObservable()
            .Where(_ => _model.ShotData._Type == Entity.Constant.ShotType.NormalPush)
            .Subscribe(_ => {
                var angle = _view.transform.eulerAngles;
                angle.z += 5.0f;
                _view.transform.eulerAngles = angle;
            })
            .AddTo(_view.gameObject);
    }

    private void SetObservableHoming() {
        // 移動
        this.UpdateAsObservable()
            .Subscribe(_ => {
                _progessFrame = Time.frameCount - _startFrame;
                Entity.HomingShotData homingData = (Entity.HomingShotData)_model.ShotData;
                if (homingData._StartHomingFrame <= _progessFrame) {
                    homingData._State = Entity.HomingShotData.HomingState.kHoming;
                }
                if (homingData._StopHomingFrame <= _progessFrame) {
                    homingData._State = Entity.HomingShotData.HomingState.kStop;
                }

                if (homingData._State == Entity.HomingShotData.HomingState.kHoming) {
                    bool isInterval = _progessFrame % homingData._HomingInterval == 0;
                    if (isInterval) {
                        _model.SetRotHoming(_model.TargetUnit.transform, homingData._AddRotDeg, _view.transform);
                    }
                }

                _model.UpdatLocalPos();
                //if (_model.LocalPos.x < -4.0f || _model.LocalPos.x > 4.0f
                //    || _model.LocalPos.y < -2.0f || _model.LocalPos.y > 4.0f) {
                //    Destroy(_view.gameObject);
                //}
            }).AddTo(_view.gameObject);

        // 画面に反映
        _model.LocalPosObservable
            .Subscribe(x => _view.UpdateLocalPos(x))
            .AddTo(_view.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == _model.TargetTag) {
            //BattlePresenter.Inst.UpdateHpBar(_model.TargetTag, _model.ShotData._Attack);
            //Destroy(gameObject);
        }
    }
}
