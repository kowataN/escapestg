﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class TimerPresenter : MonoBehaviour {
    [SerializeField] private TimerView _view = default;
    private TimerModel _model = default;
    public TimerModel Model => _model;

    private IDisposable _disposlable;

    private void Awake() {
        _model = new TimerModel();
    }

    public void Initial(int time) {
        _model.Initial(time);
        SetObservable();
    }

    public void StartTimer() {
        _model.TimerState = TimerModel.TimerStateType.Loop;
    }

    public void DisposeSubscribe() { 
        if (_disposlable != null) {
            _disposlable.Dispose();
        }
    }

    private void SetObservable() {
        // 毎秒減算
        _disposlable = Observable.Timer(
                TimeSpan.FromSeconds(_model.Interval), TimeSpan.FromSeconds(_model.Interval))
            .Where(_ => _model.TimerState == TimerModel.TimerStateType.Loop)
            .Subscribe(_ => {
                ++_model.Timer.Value;
            }).AddTo(_view.gameObject);

        // 画面更新
        _model.Timer.SubscribeToText(_view.Timer).AddTo(_view.gameObject);
    }
}
