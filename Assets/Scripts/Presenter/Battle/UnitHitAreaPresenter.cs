﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitHitAreaPresenter : MonoBehaviour {
    protected PlayerModel _model = default;
    [SerializeField] private UnitHitAreaView _view = default;

    public void Initialize(PlayerModel model) => _model = model;

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == Entity.TagName.UnitHitArea) {
            _view.SetEnabledArea(true);
        }
    }

    private void OnTriggerStay2D(Collider2D collision) {
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.tag == Entity.TagName.UnitHitArea) {
            _view.SetEnabledArea(false);
        }
    }
}
