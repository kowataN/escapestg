﻿using State;

public class StateManager {
    private StateBase _State = default;

    public void Initialize() {
        _State = new StateInitialize(StateInit);
    }

    /// <summary>
    /// ステートマシンを実行します。
    /// </summary>
    public void Execute() {
        if (_State != null) {
            _State.Execute();
        }
    }

    /// <summary>
    /// 起動初期化を行います。
    /// </summary>
    private void StateInit() {
        if (_State.StateEnd) {
            _State = new StateStartFadeIn(StateStartFadeIn);
        }

    }

    /// <summary>
    /// 開始フェードインを行います。
    /// </summary>
    private void StateStartFadeIn() {
        if (_State.StateEnd) {
            _State = new StateBattleStartEffect(StateStartEffect);
        }
    }

    /// <summary>
    /// バトル開始演出を行います。
    /// </summary>
    private void StateStartEffect() {
        if (_State.StateEnd) {
            BattlePresenter.Inst.GameStart();
            _State = new StateLoop(StateBattleLoop);
        }

    }

    /// <summary>
    /// バトルループを行います。
    /// </summary>
    private void StateBattleLoop() {
        if (_State.StateEnd) {
            _State = new StateBattleEndEffect(StateEndEffect);
        }
    }

    /// <summary>
    /// バトル終了演出を行います。
    /// </summary>
    private void StateEndEffect() {
        if (_State.StateEnd) {
            _State = new StateEndFadeOut(StateEndFadeOut);
        }
    }

    /// <summary>
    /// 終了フェードアウトを行います。
    /// </summary>
    private void StateEndFadeOut() {
    }

}
