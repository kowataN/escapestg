﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace State
{
    public class StateStartFadeIn : StateBase
    {
        private SubState _SubState = default;

        public StateStartFadeIn(executeState execute) : base(execute)
        {
            _SubState = new SubState(ExecuteInit);
        }

        public override void Execute()
        {
            _SubState.Execute();
            if (ExecDelegate != null)
            {
                ExecDelegate();
            }
        }

        private void ExecuteInit()
        {
            Fader.Inst.BlackIn(2.0f, ExecuteFadeInEnd);
            _SubState.ChangeState(ExecuteIdle);
        }
        private void ExecuteIdle()
        {
        }

        private void ExecuteFadeInEnd()
        {
            _SubState.ChangeState(null);
            StateEnd = true;
        }
    }
}
