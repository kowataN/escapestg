﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace State
{
    public class StateBattleEndEffect : StateBase
    {
        private SubState _SubState = default;

        public StateBattleEndEffect(executeState execute) : base(execute)
        {
            _SubState = new SubState(ExecuteInit);
        }

        public override void Execute()
        {
            _SubState.Execute();
            if (ExecDelegate != null)
            {
                ExecDelegate();
            }
        }

        private void ExecuteInit()
        {
            BattlePresenter.Inst.EffectView.EndCallback += ExecuteEnd;
            BattlePresenter.Inst.EffectView.PlayBattleEnd();

            _SubState.ChangeState(ExecuteIdle);
        }

        private void ExecuteIdle()
        {

        }

        private void ExecuteEnd(bool endflag)
        {
            StateEnd = true;
        }
    }
}
