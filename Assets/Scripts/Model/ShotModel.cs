﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

public class ShotModel : MonoBehaviour {
    [Header("デバッグ表示")]
    [SerializeField] private Entity.ShotData _shotData = default;
    public Entity.ShotData ShotData => _shotData;

    private GameObject _targetUnit = default;
    public GameObject TargetUnit => _targetUnit;

    public string TargetTag {
        get { return _targetUnit.tag; }
    }

    #region Position
    private Vector2 _localPos = default;
    private Vector2ReactiveProperty _rxLocalPos = default;
    public IObservable<Vector2> LocalPosObservable { get { return _rxLocalPos.AsObservable(); } }
    public Vector2 LocalPos {
        get { return _rxLocalPos.Value; }
        private set { _rxLocalPos.Value = value; }
    }
    public void UpdatLocalPos() {
        _rxLocalPos.Value += (AddPos * _shotData._MoveSpeed * Time.deltaTime);
    }
    #endregion

    #region 加算値
    [SerializeField] private Vector2 _addPos;
    public Vector2 AddPos => _addPos;

    [SerializeField] private float _backupRotDeg = 0.0f;

    public void SetRot(Vector2 targetPos) {
        Vector2 distance = targetPos - _localPos;
        float rad = Mathf.Atan2(distance.y, distance.x);
        _addPos.x = Mathf.Cos(rad);
        _addPos.y = Mathf.Sin(rad);
        _backupRotDeg = rad * Mathf.Rad2Deg;
    }

    public void SetRotWide(Vector2 targetPos, float addRot) {
        Vector2 distance = targetPos - _localPos;
        float rad = Mathf.Atan2(distance.y, distance.x) + (float)(addRot * Math.PI / 180);
        _addPos.x = Mathf.Cos(rad);
        _addPos.y = Mathf.Sin(rad);
        _backupRotDeg = rad * Mathf.Rad2Deg;
    }

    public void SetRotHoming(Transform targetTransform, float addRotDeg, Transform viewTranform) {
        // 弾と対称の角度を取得
        Vector2 targetPos = targetTransform.localPosition;
        Vector2 distance = targetPos - _localPos;
        float rotDeg = Mathf.Atan2(distance.y, distance.x) * Mathf.Rad2Deg;
        //LogView.Log("rotDeg : " + rotDeg.ToString());

        // 角度差
        var deltaAngle = Mathf.DeltaAngle(_backupRotDeg, rotDeg);
        if (Mathf.Abs(deltaAngle) < addRotDeg) {
        } else if (deltaAngle > 0) {
            rotDeg += addRotDeg;
            _backupRotDeg += addRotDeg * Mathf.Deg2Rad;
        } else {
            rotDeg -= addRotDeg;
            _backupRotDeg -= addRotDeg * Mathf.Deg2Rad;
        }

        _addPos.x = Mathf.Cos(rotDeg * Mathf.Deg2Rad);
        _addPos.y = Mathf.Sin(rotDeg * Mathf.Deg2Rad);
    }
    #endregion

    public void Initialize(Entity.ShotData shotData, GameObject targetUnit, Vector2 localPos) {
        _shotData = shotData;
        _localPos = localPos;
        _targetUnit = targetUnit;
        _rxLocalPos = new Vector2ReactiveProperty(_localPos);
    }
}
