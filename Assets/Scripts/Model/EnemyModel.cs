using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyModel
{
    private Vector2 _startPos = default;
    private Vector2 _movePos = default;
    private int _shotId = default;
    private int _aliveTime = default;

    public EnemyModel(int aliveTime, Vector2 startPos, Vector2 movePos, int shotId) {
        _aliveTime = aliveTime;
        _startPos = startPos;
        _movePos = movePos;
        _shotId = shotId;
    }
}
