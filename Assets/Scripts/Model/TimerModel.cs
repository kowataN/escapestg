﻿using UniRx;
using UnityEngine;

public class TimerModel
{
    private int _interval = 1;
    public int Interval => _interval;

    private IntReactiveProperty _timer = null;
    public IntReactiveProperty Timer => _timer;

    public enum TimerStateType { Idle, Loop };
    public TimerStateType TimerState { get; set; }

    public void Initial(int time)
    {
        if (_timer == null) {
            _timer = new IntReactiveProperty(time);
        } else {
            _timer.Value = time;
        }
        TimerState = TimerStateType.Idle;
    }
}
