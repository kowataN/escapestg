﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class PlayerModel {
    #region position
    private Vector2 _initialLocalPos = default;
    private Vector2 _latestLocalPos = default;

    private Vector2ReactiveProperty _rxLocalPos = default;
    public IObservable<Vector2> LocalPosObservable { get { return _rxLocalPos.AsObservable(); } }

    public Vector2 LocalPos {
        get { return _rxLocalPos.Value; }
        private set {
            _latestLocalPos = _rxLocalPos.Value;
            {
                _rxLocalPos.Value = new Vector2(
                    Mathf.Clamp(value.x, -2.2f, 2.2f),
                    Mathf.Clamp(value.y, -1.05f, 2.6f));
            }
        }
    }

    public Vector2 LatestLocalPos {
        get { return _latestLocalPos; }
        set { _latestLocalPos = value; }
    }
    #endregion

    public void Initialize(Vector2 localPos) {
        _latestLocalPos = _initialLocalPos = localPos;
        _rxLocalPos = new Vector2ReactiveProperty(_initialLocalPos);
    }

    /// <summary>
    /// ローカルポジションを設定します。
    /// </summary>
    /// <param name="value"></param>
    public void SetLocalPos(Vector2 value) => LocalPos = value;
    public void AddLoadPos(Vector2 value) => SetLocalPos(LocalPos + (value * 3.0f));
}
