using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class ScoreModel {
    private LongReactiveProperty _score;
    public LongReactiveProperty Score => _score;

    public void Initialize(long score) {
        _score = new LongReactiveProperty(score);
    }
}
