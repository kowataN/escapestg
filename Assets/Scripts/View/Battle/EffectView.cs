﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EffectView : MonoBehaviour {
    [SerializeField] Animator _animator = default;
    public Action<bool> EndCallback = default;

    public void PlayBattleStart() {
        this.gameObject.SetActive(true);
        _animator.Play("BattleStart", 0, 0.0f);
    }

    public void PlayBattleEnd() {
        this.gameObject.SetActive(true);
        _animator.Play("BattleEnd", 0, 0.0f);
    }

    public void EndAnimation() {
        this.gameObject.SetActive(false);
        EndCallback?.Invoke(true);
    }
}
