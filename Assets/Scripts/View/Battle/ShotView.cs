﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotView : MonoBehaviour {
    [SerializeField] private Transform _transform = default;
    public Transform Transform => _transform;

    public void UpdateLocalPos(Vector2 value) => _transform.localPosition = value;
}
