﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitHitAreaView : MonoBehaviour {
    [SerializeField] private SpriteRenderer _sprite = default;

    void Start() {
        _sprite.enabled = false;
    }

    public void SetEnabledArea(bool value) => _sprite.enabled = value;
}
