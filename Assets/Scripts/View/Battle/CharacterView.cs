﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterView : MonoBehaviour {
    [SerializeField] private Transform _transform = default;
    [SerializeField] private Transform _shotPos = default;
    [SerializeField] private SpriteRenderer _sprite = default;

    public SpriteRenderer Sprite { get { return _sprite; } }

    public Transform ShotPos { get { return _shotPos; } }

    public void UpdateLocalPos(Vector2 value) => _transform.localPosition = value;

    public void UpdateDirection(Vector2 diff) {
        _transform.rotation = Quaternion.FromToRotation(Vector2.up, diff);
    }
}
