﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Entity
{
    public static class TagName
    {
        public const string PlayerShip = "PlayerShip";
        public const string PlayerBullet = "PlayerBullet";
        public const string PlayerHpGage = "PlayerHpGage";
        public const string PlayerKasuriArea = "PlayerKasuriArea";

        public const string EnemyShip = "EnemyShip";
        public const string EnemyBullet = "EnemyBullet";
        public const string EnemyHpGage = "EnemyHpGage";
        public const string EnemyKasuriArea = "EnemyKasuriArea";

        public const string UnitHitArea = "PlayerUnitArea";
    }
}
