﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Entity
{
    [CreateAssetMenu(fileName = "Data", menuName = "Entity/CreateShotWide")]
    public class WideShotData : ShotData
    {
        [Header("n-WAY:奇数のみ")]
        public int _WayCount = 3;

        [Header("間隔")]
        public float _AddRot = 5.0f;
    }
}

