﻿using UnityEngine;

namespace Entity
{
    [CreateAssetMenu(fileName = "Data", menuName = "Entity/CreateShotHoming")]
    public class HomingShotData : ShotData
    {
        public enum HomingState
        {
            kIdle, kHoming, kStop
        }

        [Header("追尾開始フレーム数")]
        public float _StartHomingFrame = 10.0f;
        [Header("追尾補正間隔")]
        public float _HomingInterval = 20.0f;
        [Header("加算値:度数表記")]
        public float _AddRotDeg = 0;

        [Header("追尾停止フレーム数")]
        public float _StopHomingFrame = 90.0f;

        [Header("追尾状態")]
        public HomingState _State = HomingState.kIdle;
    }
}
