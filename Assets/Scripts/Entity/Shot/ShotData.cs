﻿using UnityEngine;

namespace Entity
{
    [CreateAssetMenu(fileName = "Data", menuName = "Entity/CreateShot")]
    public class ShotData : ScriptableObject
    {
        [Header("種類")]
        public Entity.Constant.ShotType _Type = Entity.Constant.ShotType.Normal;

        [Header("攻撃力")]
        public int _Attack = 100;

        [Header("弾速")]
        public float _MoveSpeed = 3.0f;

        [Header("消費ゲージ")]
        public int _UseGage = 0;
    }
}
